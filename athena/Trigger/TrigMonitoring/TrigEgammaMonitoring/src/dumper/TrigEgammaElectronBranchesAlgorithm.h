/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/
#ifndef TrigEgammaElectronBranchesAlgorithm_H
#define TrigEgammaElectronBranchesAlgorithm_H

#include "../TrigEgammaMonitorBaseAlgorithm.h"
#include "AthenaMonitoringKernel/IMonitoredVariable.h"
#include "AthenaMonitoringKernel/Monitored.h"



class TrigEgammaElectronBranchesAlgorithm: public TrigEgammaMonitorBaseAlgorithm 
{

  public:

    TrigEgammaElectronBranchesAlgorithm( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~TrigEgammaElectronBranchesAlgorithm() override;

    virtual StatusCode initialize() override;
    
  protected:

    void fillLabel( const ToolHandle<GenericMonitoringTool>& groupHandle, const std::string &histname, const std::string &label ) const;
    
    
    void fillFastCalo( const ToolHandle<GenericMonitoringTool>& groupHandle, const xAOD::TrigEMCluster *cl ) const;
    void fillOffline( const ToolHandle<GenericMonitoringTool>& groupHandle, const xAOD::Electron *el ) const;


};

#endif
